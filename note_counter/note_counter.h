#ifndef NOTE_COUNTER_H
#define NOTE_COUNTER_H

#include <fstream>
#include <vector>
#include <string>
#include <iostream>


using std::cin;
using std::ifstream;
using std::string;
using std::getline;
using std::vector;



vector<char> plain_notes(std::string filepath)
{
    std::vector<char> plain_notes;
    std::ifstream mxmlfile(filepath);
    std::string current_line;

    if (mxmlfile.is_open())
    {
        while (std::getline(mxmlfile, current_line))
        {
            if (current_line.rfind("<step>", 0) == 0){
                plain_notes.push_back(current_line[6]);
            }
        }
        mxmlfile.close();
    }
    else
        std::cerr << "Unable to find the music xml file at current path:" << filepath << std::endl;
    return plain_notes;
}

#endif // NOTE_COUNTER_H
