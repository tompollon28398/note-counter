#ifndef NOTES_H
#define NOTES_H


#define C_NOTE 0
#define DB_NOTE 1
#define D_NOTE 2
#define EB_NOTE 3
#define E_NOTE 4
#define F_NOTE 5
#define GB_NOTE 6
#define G_NOTE 7
#define AB_NOTE 8
#define A_NOTE 9
#define BB_NOTE 10
#define B_NOTE 11

#endif // NOTES_H
