#ifndef NOTE_ALGORITHM_H
#define NOTE_ALGORITHM_H
#include <notes.h>
#include <vector>
#include <string>

class Note{
    int note_number=0;
    int number_of_occurrences=0;

public:
    Note(){
        set_note_number(0);
        set_number_of_occurrences(0);
    }
    int get_octave(){
        return note_number/12;
    }
    int get_note_name(){
        return note_number%12;
    }
    int get_occurrences(){
        return number_of_occurrences;
    }
    int get_note_number(){
        return note_number;
    }
    void add_occurrence(){
        number_of_occurrences++;
    }

    void add_occurrence_timing(int timing){
        number_of_occurrences = number_of_occurrences + timing;
    }
    void set_note_number(int number){
        this->note_number = number;
    }
    void set_number_of_occurrences(int number){
        this->number_of_occurrences = number;
    };
    std::string print(){
        std::string s_number = std::to_string(get_note_number());
        std::string s_occurrences = std::to_string(get_occurrences());
        return s_number + ";" + s_occurrences + "\n";
    }
};

std::vector<Note> notes(std::vector<int> notes){
    std::vector<Note> notes_vector;
    for (auto number: notes){
        if(notes_vector.size() == 0){
            Note new_note;
            new_note.set_note_number(number);
            new_note.add_occurrence();
            notes_vector.push_back(new_note);
        }else{
            bool found = false;
            int pos = 0;
            for(auto note: notes_vector){
                if (note.get_note_number() == number){
                    notes_vector.at(pos).add_occurrence();
                    found = true;
                    break;
                }
                pos++;
            }
            if(found == false){
                Note new_note;
                new_note.set_note_number(number);
                new_note.add_occurrence();
                notes_vector.push_back(new_note);
            }
        }
    }
    return notes_vector;
}
#endif // NOTE_ALGORITHM_H
