#ifndef MXMLPARSER_H
#define MXMLPARSER_H

#include <fstream>
#include <vector>
#include <string>
#include <iostream>
#include <notes.h>
#include <note_algorithm.h>

using std::cin;
using std::ifstream;
using std::string;
using std::getline;
using std::vector;


vector<int> plain_notes(std::string filepath)
{
    std::vector<int> plain_notes;
    std::ifstream mxmlfile(filepath);
    std::string current_line;

    if (mxmlfile.is_open())
    {
        while (std::getline(mxmlfile, current_line))
        {
            if (current_line.rfind("          <step>") == 0){
                int note = -100;
                switch(current_line[16]) {
                case 'A':
                    note = A_NOTE;
                    break;
                case 'B':
                    note = B_NOTE;
                    break;
                case 'C':
                    note = C_NOTE;
                    break;
                case 'D':
                    note = D_NOTE;
                    break;
                case 'E':
                    note = E_NOTE;
                    break;
                case 'F':
                    note = F_NOTE;
                    break;
                case 'G':
                    note = G_NOTE;
                    break;
                default:
                    std::cerr << "Unexpected note" << std::endl;
                    break;
                }
                std::getline(mxmlfile, current_line);
                if (current_line.rfind("          <alter>-1</alter>") == 0){
                    note--;
                    std::getline(mxmlfile, current_line);
                } else if (current_line.rfind("          <alter>1</alter>") == 0){
                    note++;
                    std::getline(mxmlfile, current_line);
                }
                if (current_line.rfind("          <octave>") == 0){
                    note = note + (current_line[18] - 48)*12;
                }
                plain_notes.push_back(note);
            }
        }
        mxmlfile.close();
    }
    else
        std::cerr << "Unable to find the music xml file at current path:" << filepath << std::endl;
    return plain_notes;
}


vector<int> plain_notes_w_timing(std::string filepath)
{
    std::vector<int> plain_notes;
    std::ifstream mxmlfile(filepath);
    std::string current_line;

    if (mxmlfile.is_open())
    {
        while (std::getline(mxmlfile, current_line))
        {
            if (current_line.rfind("          <step>") == 0){
                int note = -100;
                switch(current_line[16]) {
                case 'A':
                    note = A_NOTE;
                    break;
                case 'B':
                    note = B_NOTE;
                    break;
                case 'C':
                    note = C_NOTE;
                    break;
                case 'D':
                    note = D_NOTE;
                    break;
                case 'E':
                    note = E_NOTE;
                    break;
                case 'F':
                    note = F_NOTE;
                    break;
                case 'G':
                    note = G_NOTE;
                    break;
                default:
                    std::cerr << "Unexpected note" << std::endl;
                    break;
                }
                std::getline(mxmlfile, current_line);
                if (current_line.rfind("          <alter>-1</alter>") == 0){
                    note--;
                    std::getline(mxmlfile, current_line);
                } else if (current_line.rfind("          <alter>1</alter>") == 0){
                    note++;
                    std::getline(mxmlfile, current_line);
                }
                if (current_line.rfind("          <octave>") == 0){
                    note = note + (current_line[18] - 48)*12;
                }
                plain_notes.push_back(note);
            }
        }
        mxmlfile.close();
    }
    else
        std::cerr << "Unable to find the music xml file at current path:" << filepath << std::endl;
    return plain_notes;
}



#endif // MXMLPARSER_H
