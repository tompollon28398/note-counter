#include <iostream>
#include <fstream>
#include <mxmlparser.h>
#include <note_algorithm.h>
using namespace std;

int main()
{
    std::vector<int> notes_int = plain_notes("/home/tommaso/Desktop/TESI/xml/Lachner_horn.xml");
    std::vector<Note> note_Vector = notes(notes_int);
    ofstream myfile;
    myfile.open ("/home/tommaso/Desktop/TESI/xml/export/lhron.csv");
    for (auto i: note_Vector){
        std::cout << i.print();
        myfile << i.print();
    }
    myfile.close();
    return 0;
}

